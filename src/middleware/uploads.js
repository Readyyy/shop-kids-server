const fs       = require('fs');
const path     = require('path');
const dir      = __dirname.substring(0, __dirname.lastIndexOf('/src')) + '/public/assets/images/';
module.exports = (req, res, next) => {
  let app      = req.app;
  let nameFile = req.file.filename + path.extname(req.file.originalname);
  fs.rename(dir + req.file.filename, dir + nameFile, (err) => {
    if (!err) {
      let image = {
        url: 'assets/images/' + nameFile,
      };
      if (!isNaN(parseInt(req.params.id)))
        image.idProductId = req.params.id;
      app.service('image').create(image).then(() => res.json({uri: 'assets/images/' + nameFile}))
        .catch(console.log);
    } else next(err);
  });
};
