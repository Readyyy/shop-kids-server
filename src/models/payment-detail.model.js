const Sequelize = require('sequelize');

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient');
  const paymentDetail = sequelizeClient.define('payment-detail', {
    address: {
      type: Sequelize.STRING,
      allowNull: false
    },
    phone: {
      type: Sequelize.STRING,
      allowNull: false
    },
    idUser: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate(models) { // eslint-disable-line no-unused-vars

      }
    }
  });

  return paymentDetail;
};
