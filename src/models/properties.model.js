const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('properties', {
    name: {
      type      : Sequelize.STRING,
      primaryKey: true,
      allowNull : false
    },
    status : {
      type        : Sequelize.BOOLEAN,
      defaultValue: 1,
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate (models) { // eslint-disable-line no-unused-vars
        this.belongsToMany(models['products'], {through: models['properties-detail']});
      }
    }
  });
};
