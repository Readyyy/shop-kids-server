const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('payment-methods', {
    name       : {
      type      : Sequelize.STRING,
      allowNull : false,
      primaryKey: true
    },
    description: {
      type: Sequelize.STRING
    },
    status     : {
      type        : Sequelize.BOOLEAN,
      defaultValue: 1
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate(models) { // eslint-disable-line no-unused-vars
      }
    }
  });
};
