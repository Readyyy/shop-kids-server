const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('feedback', {
    text     : {
      type     : Sequelize.STRING,
      allowNull: false
    },
    rate     : {
      type     : Sequelize.INTEGER,
      allowNull: false
    },
    idProduct: {
      type      : Sequelize.INTEGER,
      allowNull : false,
      references: {
        model: 'products',
        key  : 'id'
      }
    },
    idUser   : {
      type      : Sequelize.INTEGER,
      allowNull : false,
      references: {
        model: 'users',
        key  : 'id'
      }
    },
    status : {
      type        : Sequelize.BOOLEAN,
      defaultValue: 1,
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate (models) { // eslint-disable-line no-unused-vars
      }
    }
  });
};
