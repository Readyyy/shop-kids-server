const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('bill', {
    idUser : {
      type      : Sequelize.INTEGER,
      allowNull : false,
      references: {
        model: 'users',
        key  : 'id'
      }
    },
    payment: {
      type      : Sequelize.STRING,
      allowNull : false,
      references: {
        model: 'payment-methods',
        key  : 'name'
      }
    },
    address: {
      type     : Sequelize.STRING,
      allowNull: false
    },
    phone  : {
      type: Sequelize.STRING
    },
    status : {
      type        : Sequelize.BOOLEAN,
      defaultValue: 1,
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate(models) { // eslint-disable-line no-unused-vars
        this.belongsToMany(models['products'], {through: models['bill-detail']});
      }
    }
  });
};
