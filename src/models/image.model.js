const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('image', {
    url: {
      type     : Sequelize.STRING,
      allowNull: false,
    },
    status : {
      type        : Sequelize.BOOLEAN,
      defaultValue: 1,
    }
    // ,
    // idProduct: {
    //   type      : Sequelize.INTEGER,
    //   allowNull : false,
    //   references: {
    //     model: 'products',
    //     key  : 'id'
    //   }
    // }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate (models) { // eslint-disable-line no-unused-vars
        // this.belongsTo(models['products'], {as: 'idProduct'});
      }
    }
  });
};
