const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('products', {
    name       : {
      type     : Sequelize.STRING,
      allowNull: false
    },
    description: {
      type: Sequelize.STRING,
    },
    price      : {
      type     : Sequelize.INTEGER,
      allowNull: false
    },
    status : {
      type        : Sequelize.BOOLEAN,
      defaultValue: 1,
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate (models) { // eslint-disable-line no-unused-vars
        this.belongsToMany(models['properties'], {through: models['properties-detail']});
        this.belongsToMany(models['bill'], {through: models['bill-detail']});
        this.hasMany(models['image'], {as: 'idProduct'});
      }
    }
  });
};
