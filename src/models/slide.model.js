const Sequelize = require('sequelize');

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('slide', {
    url: {
      type: Sequelize.STRING,
      allowNull: false
    },
    idProduct: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'products',
        key: 'id'
      }
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate(models) { // eslint-disable-line no-unused-vars
      }
    }
  });
};
