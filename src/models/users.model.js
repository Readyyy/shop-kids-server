const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('users', {
    email   : {
      type     : Sequelize.STRING,
      allowNull: false
    },
    fullName: {
      type     : Sequelize.STRING,
      allowNull: false
    },
    password: {
      type     : Sequelize.STRING,
      allowNull: false
    },
    address : {
      type: Sequelize.STRING
    },
    birthDay: {
      type: Sequelize.STRING
    },
    level   : {
      type: Sequelize.INTEGER
    },
    status  : {
      type        : Sequelize.BOOLEAN,
      defaultValue: 1
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate(models) { // eslint-disable-line no-unused-vars
        this.belongsToMany(models['bill'], {through: models['bill-detail']});
      }
    }
  });
};
