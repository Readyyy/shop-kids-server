const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  return sequelizeClient.define('bill-detail', {
    price   : {
      type     : Sequelize.INTEGER,
      allowNull: false
    },
    quantity: {
      type     : Sequelize.INTEGER,
      allowNull: false
    }
  }, {
    charset     : 'utf8',
    collate     : 'utf8_unicode_ci',
    classMethods: {
      associate (models) { // eslint-disable-line no-unused-vars
      }
    }
  });
};
