'use strict';

// Initializes the `propertiesDetail` service on path `/propertiesDetail`
const createService = require('feathers-sequelize');
const createModel = require('../../models/properties-detail.model');
const hooks = require('./properties-detail.hooks');
const filters = require('./properties-detail.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'properties-detail',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/propertiesDetail', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('propertiesDetail');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
