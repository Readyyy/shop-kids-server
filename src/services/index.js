const users            = require('./users/users.service.js');
const bill             = require('./bill/bill.service.js');
const billDetail       = require('./bill-detail/bill-detail.service.js');
const product          = require('./product/product.service.js');
const slide            = require('./slide/slide.service.js');
const feedback         = require('./feedback/feedback.service.js');
const image            = require('./image/image.service.js');
const properties       = require('./properties/properties.service.js');
const paymentMethods   = require('./payment-methods/payment-methods.service.js');
const paymentDetail    = require('./payment-detail/payment-detail.service.js');
const propertiesDetail = require('./properties-detail/properties-detail.service.js');
const billCustomer     = require('./billCustomer/billCustomer');
module.exports         = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(bill);
  app.configure(billDetail);
  app.configure(product);
  app.configure(slide);
  app.configure(feedback);
  app.configure(image);
  app.configure(properties);
  app.configure(paymentMethods);
  app.configure(paymentDetail);
  app.configure(propertiesDetail);
  app.configure(billCustomer);
};
