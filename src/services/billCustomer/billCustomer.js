const hook     = require('./billCustomer.hook');
module.exports = function () {
  const app = this;
  app.use('/billCustomer', {
    find(params) {
      let user             = params.user.dataValues;
      let {bill, products} = app.get('sequelizeClient').models;
      return bill.findAll({
        include: [{
          model  : products,
          through: {attributes: ['price', 'quantity']}
        }],
        where  : {
          idUser: user.id
          // status: 1
        }
      });
    },
    get(id, params) {
    },
    create(data, params) {
    },
    update(id, data, params) {
    },
    patch(id, data, params) {
    },
    remove(id, params) {
    },
    setup(app, path) {
    }
  });
  const service = app.service('billCustomer');
  service.hooks(hook);
};
