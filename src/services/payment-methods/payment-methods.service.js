'use strict';

// Initializes the `paymentMethods` service on path `/payment-methods`
const createService = require('feathers-sequelize');
const createModel = require('../../models/payment-methods.model');
const hooks = require('./payment-methods.hooks');
const filters = require('./payment-methods.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'payment-methods',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/payment-methods', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('payment-methods');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
