'use strict';


module.exports = {
  before: {
    all   : [],
    find  : [],
    get   : [],
    create: [],
    update: [],
    patch : [],
    remove: []
  },

  after: {
    all   : [],
    find  : [],
    get   : [],
    create: [(hook) => {
      let service     = hook.app.service('image');
      let idProductId = hook.result.dataValues.id;
      service.patch(null, {
        idProductId
      }, {
        query: {
          idProductId: null
        }
      });
    }],
    update: [],
    patch : [],
    remove: [(hook) => {
      let service     = hook.app.service('image');
      let idProductId = hook.result.dataValues.id;
      // service.remove(null, {query: {idProductId}});
    }]
  },

  error: {
    all   : [],
    find  : [],
    get   : [],
    create: [],
    update: [],
    patch : [],
    remove: [(hook) => {
      let {service, id} = hook;
      service.patch(id, {status: false});
    }]
  }
};
