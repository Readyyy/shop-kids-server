'use strict';

// Initializes the `billDetail` service on path `/billDetail`
const createService = require('feathers-sequelize');
const createModel = require('../../models/bill-detail.model');
const hooks = require('./bill-detail.hooks');
const filters = require('./bill-detail.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'bill-detail',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/billDetail', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('billDetail');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
