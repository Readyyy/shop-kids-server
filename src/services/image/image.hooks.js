'use strict';

const {authenticate} = require('feathers-authentication').hooks;
const fs             = require('fs');
const path           = require('path');
const __dirPublic    = path.join(__dirname, '..', '..', '..', 'public');
module.exports       = {
  before: {
    all   : [],
    find  : [],
    get   : [],
    create: [authenticate('jwt')],
    update: [authenticate('jwt')],
    patch : [authenticate('jwt')],
    remove: [authenticate('jwt')]
  },

  after: {
    all   : [],
    find  : [],
    get   : [],
    create: [],
    update: [],
    patch : [],
    remove: [(hook) => fs.unlinkSync(path.join(__dirPublic, hook.result.dataValues.url))]
  },

  error: {
    all   : [],
    find  : [],
    get   : [],
    create: [],
    update: [],
    patch : [],
    remove: []
  }
};
