'use strict';

// Initializes the `image ` service on path `/image`
const createService = require('feathers-sequelize');
const createModel = require('../../models/image.model');
const hooks = require('./image.hooks');
const filters = require('./image.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'image',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/image', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('image');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
