'use strict';

// Initializes the `paymentDetail` service on path `/paymentDetail`
const createService = require('feathers-sequelize');
const createModel = require('../../models/payment-detail.model');
const hooks = require('./payment-detail.hooks');
const filters = require('./payment-detail.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'payment-detail',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/paymentDetail', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('paymentDetail');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
