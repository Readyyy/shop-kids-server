'use strict';

const assert = require('assert');
const app = require('../../src/app');

describe('\'billDetail\' service', () => {
  it('registered the service', () => {
    const service = app.service('bill-detail');

    assert.ok(service, 'Registered the service');
  });
});
