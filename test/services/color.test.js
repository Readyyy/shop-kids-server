'use strict';

const assert = require('assert');
const app = require('../../src/app');

describe('\'color \' service', () => {
  it('registered the service', () => {
    const service = app.service('color');

    assert.ok(service, 'Registered the service');
  });
});
