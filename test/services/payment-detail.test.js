'use strict';

const assert = require('assert');
const app = require('../../src/app');

describe('\'paymentDetail\' service', () => {
  it('registered the service', () => {
    const service = app.service('payment-detail');

    assert.ok(service, 'Registered the service');
  });
});
