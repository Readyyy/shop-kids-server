'use strict';

const assert = require('assert');
const app = require('../../src/app');

describe('\'slide\' service', () => {
  it('registered the service', () => {
    const service = app.service('slide');

    assert.ok(service, 'Registered the service');
  });
});
