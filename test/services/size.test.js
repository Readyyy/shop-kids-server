'use strict';

const assert = require('assert');
const app = require('../../src/app');

describe('\'size \' service', () => {
  it('registered the service', () => {
    const service = app.service('size');

    assert.ok(service, 'Registered the service');
  });
});
