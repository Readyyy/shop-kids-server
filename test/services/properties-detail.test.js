'use strict';

const assert = require('assert');
const app = require('../../src/app');

describe('\'propertiesDetail\' service', () => {
  it('registered the service', () => {
    const service = app.service('properties-detail');

    assert.ok(service, 'Registered the service');
  });
});
